﻿using DubaiSmoke.Users.Domain.Entities;

namespace DubaiSmoke.Users.Domain.Repositories
{
    public interface IUserRepository : IRepository<UserEntity>
    {

    }
}
